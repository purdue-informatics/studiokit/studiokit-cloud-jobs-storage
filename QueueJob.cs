﻿using Microsoft.WindowsAzure.Storage.Queue;
using StudioKit.Cloud.Jobs.Exceptions;
using StudioKit.Cloud.Jobs.Interfaces;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.Diagnostics;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Cloud.Jobs.Storage
{
	public abstract class QueueJob<TQueueMessage> : IJob
		where TQueueMessage : IQueueMessage
	{
		private readonly IErrorHandler _errorHandler;

		private readonly ILogger _logger;

		private readonly IQueueManager<TQueueMessage> _queueManager;

		protected TimeSpan DefaultVisibilityTimeout { get; set; }

		protected int DequeueLimit { get; set; }

		protected string JobName => GetType().FullName;

		protected QueueJob(IQueueManager<TQueueMessage> queueManager, ILogger logger, IErrorHandler errorHandler)
		{
			if (queueManager == null)
				throw new ArgumentNullException(nameof(queueManager));
			if (logger == null)
				throw new ArgumentNullException(nameof(logger));
			if (errorHandler == null)
				throw new ArgumentNullException(nameof(errorHandler));

			_queueManager = queueManager;
			_logger = logger;
			_errorHandler = errorHandler;

			DefaultVisibilityTimeout = TimeSpan.FromSeconds(30);
			DequeueLimit = 5;
		}

		public async Task ExecuteAsync()
		{
			var message = await GetMessageAsync();
			if (message == null)
				return;
			try
			{
				if (message.CloudQueueMessage.DequeueCount >= DequeueLimit)
				{
					await _queueManager.DeleteMessageAsync(message);
					var errorMessage = $"Message (Id = {message.CloudQueueMessage.Id}, Message = {message.CloudQueueMessage.AsString}) reached dequeue limit of {DequeueLimit}. Message deleted.";
					_logger.Error(errorMessage);
					_errorHandler.CaptureException(new JobException(errorMessage));
					return;
				}
				_logger.Debug($"{JobName} starting");
				await ProcessMessageAsync(message);
				_logger.Debug($"{JobName} succeeded");
			}
			catch (Exception e)
			{
				var errorMessage = $"{JobName} failed. {e.Message}";
				_logger.Error(errorMessage);
				_errorHandler.CaptureException(new JobException(errorMessage, e));
				await RequeueMessageAsync(message);
			}
		}

		private async Task<TQueueMessage> GetMessageAsync()
		{
			var cancellationTokenSource = new CancellationTokenSource();
			var cancellationToken = cancellationTokenSource.Token;
			try
			{
				var message = await _queueManager.GetMessageAsync(DefaultVisibilityTimeout, null, null, cancellationToken);
				if (message != null)
					_logger.Debug($"Retrieved message (Id = {message.CloudQueueMessage.Id}, Message = {message.CloudQueueMessage.AsString}, VisibilityTimeout = {DefaultVisibilityTimeout})");
				return message;
			}
			catch (Exception e)
			{
				var message = $"{JobName} failed to retrieve queue message: ({e.Message})";
				_logger.Error(message);
				_errorHandler.CaptureException(new JobException(message, e));
				return default(TQueueMessage);
			}
		}

		private async Task RequeueMessageAsync(TQueueMessage message)
		{
			try
			{
				var visibilityTimeout = TimeSpan.FromSeconds(message.CloudQueueMessage.DequeueCount * 30);
				await _queueManager.UpdateMessageAsync(message, visibilityTimeout, MessageUpdateFields.Visibility);
			}
			catch (Exception e)
			{
				var errorMessage = $"Requeue message (Id = {message.CloudQueueMessage.Id}, Message = {message.CloudQueueMessage.AsString}) failed. {e.Message}";
				_logger.Error(errorMessage);
				_errorHandler.CaptureException(new JobException(errorMessage, e));
			}
		}

		/// <summary>
		/// Process a queue message of type <see cref="TQueueMessage"/>. Checks if message was successfully processed and will delete from Queue.
		/// </summary>
		/// <param name="message"></param>
		private async Task ProcessMessageAsync(TQueueMessage message)
		{
			_logger.Info("Process message {0}", message.CloudQueueMessage.Id);
			var cancellationTokenSource = new CancellationTokenSource();
			var cancellationToken = cancellationTokenSource.Token;

			var processed = await ExecuteJobAsync(message, cancellationToken);
			_logger.Info($"Message {message.CloudQueueMessage.Id} processed = {processed}");
			if (!processed)
				return;

			_logger.Info($"Message {message.CloudQueueMessage.Id} deleting");
			await _queueManager.DeleteMessageAsync(message, cancellationToken);
			_logger.Info($"Message {message.CloudQueueMessage.Id} deleted");
		}

		protected abstract Task<bool> ExecuteJobAsync(TQueueMessage message, CancellationToken cancellationToken);
	}
}